package com.company;

import java.util.Scanner;

public class homeassignment3 {

    public static void main(String[] args) {
        Task1();
        Task2();
        Task3();
        Task3dot1();
        Task4();
    }

    public static void Task1() {
        //1. Ввести n строк с консоли, найти самую короткую строку. Вывести эту строку и ее длину.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Task 1");

        System.out.println("How many rows do you want to fill in?");
        int countOfRows = Integer.parseInt(scanner.nextLine());

        if (countOfRows > 0) {
            String[] arrayOfRows = new String[countOfRows];

            for (int i = 0; i < countOfRows; i++) {
                System.out.println(String.format("Enter the row number %d: ", i + 1));
                String row = scanner.nextLine();
                arrayOfRows[i] = row;
            }

            int minLengthOfRow = arrayOfRows[0].length();
            String minLengthRow = arrayOfRows[0];

            for (int i = 0; i < countOfRows; i++) {
                if (arrayOfRows[i].length() <= minLengthOfRow) {
                    minLengthRow = arrayOfRows[i];
                    minLengthOfRow = arrayOfRows[i].length();
                }
            }
            System.out.println("Минимальная строка из введенных = " + minLengthRow + " ,Размер строки = " + minLengthOfRow);
        } else {
            System.out.println("Count of rows should be more then 0");
            Task1();
        }

    }

    public static void Task2() {
        //2. Ввести n строк с консоли. Вывести на консоль те строки, длина которых меньше средней.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Task 2");
        System.out.println("How many rows do you want to fill in? (Not less then 2)");
        int countOfRows = Integer.parseInt(scanner.nextLine());

        if (countOfRows >= 2) {
            String[] arrayOfRows = new String[countOfRows];

            for (int i = 0; i < countOfRows; i++) {
                System.out.println(String.format("Enter the row number %d: ", i + 1));
                String row = scanner.nextLine();
                arrayOfRows[i] = row;
            }
            int summOfRowsLength = 0;
            for (int i = 0; i < countOfRows; i++) {
                summOfRowsLength += arrayOfRows[i].length();
                if (i == countOfRows - 1) {
                    summOfRowsLength /= countOfRows;
                }
            }
            for (int i = 0; i < countOfRows; i++) {
                if (arrayOfRows[i].length() < summOfRowsLength) {
                    System.out.println("Строки с количеством символов меньше среднего: " + arrayOfRows[i]);
                }
            }


        } else {
            System.out.println("Count of rows should be at least 2");
            Task2();
        }

    }
    public static void Task3() {
        //Ввести n строк с консоли. Вывести на консоль те строки, которые содержат ключевое слово (ключевое слово задано изначально, например “tree”)
        Scanner scanner = new Scanner(System.in);

        System.out.println("Task 3");
        System.out.println("How many rows do you want to fill in?");
        int countOfRows = Integer.parseInt(scanner.nextLine());
        String keyWord = "tree";

        if (countOfRows > 0) {
            String[] arrayOfRows = new String[countOfRows];

            for (int i = 0; i < countOfRows; i++) {
                System.out.println(String.format("Enter the row number %d: ", i + 1));
                String row = scanner.nextLine();
                arrayOfRows[i] = row;
            }
            for (int i = 0; i < countOfRows; i++) {
                if (arrayOfRows[i].contains(keyWord)) {
                    System.out.println("Строка содержащее ключевое слово " + keyWord + " == " + arrayOfRows[i]);
                }
            }

        } else {
            System.out.println("Count of rows should be more then 0");
            Task2();
        }
    }
    public static void Task3dot1() {
        //3.1* Вывести на консоль те строки, которые содержат одно из заданных ключевых слов (ключевые слова заданы изначально, в массиве например “tree”, “meat”).
        Scanner scanner = new Scanner(System.in);

        System.out.println("Task 3.1*");
        System.out.println("How many rows do you want to fill in?");
        int countOfRows = Integer.parseInt(scanner.nextLine());
        String keyWord = "tree";
        String keyWord2 = "tree";

        if (countOfRows > 0) {
            String[] arrayOfRows = new String[countOfRows];

            for (int i = 0; i < countOfRows; i++) {
                System.out.println(String.format("Enter the row number %d: ", i + 1));
                String row = scanner.nextLine();
                arrayOfRows[i] = row;
            }
            for (int i = 0; i < countOfRows; i++) {
                if (arrayOfRows[i].contains(keyWord) || arrayOfRows[i].contains(keyWord2)) {
                    System.out.println("Строка содержащее ключевое слово " + keyWord + " == " + arrayOfRows[i]);
                }
            }

        } else {
            System.out.println("Count of rows should be more then 0");
            Task2();
        }
    }
    public static void Task4() {
        //4** Ввести с консоли несколько предложений. Удалить в них все слова заданной длины, начинающиеся на согласную букву.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Task 4**");
        System.out.println("How many rows do you want to fill in?");
        int countOfRows = Integer.parseInt(scanner.nextLine());


        if (countOfRows > 0) {
            String[] arrayOfRows = new String[countOfRows];
            String[] cleanArray = new String[countOfRows];

            for (int i = 0; i < countOfRows; i++) {
                System.out.println(String.format("Enter the row number %d: ", i + 1));
                String row = scanner.nextLine();
                arrayOfRows[i] = row.replaceAll("(\\.|.!|\\;|\\:|\\?|\\,)", "").toLowerCase();
                arrayOfRows[i] = row.replaceAll("\\b[bcdfghjklmnpqrstvwxyz]\\w{3}", "");
                System.out.println(arrayOfRows[i]);
            }

        } else {
            System.out.println("Count of rows should be more then 0");
            Task2();
        }
    }
}
