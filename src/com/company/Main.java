package com.company;

public class Main {

    public static void main(String[] args) {

//        4*. Написать программу, которая будет проверять, является ли строка словом палиндромом. Программа должна определять палиндром, даже если в
//        слове есть буквы разного регистра (например “Шалаш”, “ПоТоП”). Результат проверки (палиндром или нет) выводить в консоль.
//        4.1** Определять не только слова, но и предложения. Учесть, что предложения могут содержать
//        знаки пунктуации (например “Аргентина манит негра!”).

            System.out.println("Task 4.1**");
            String text = "Lorol Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's non 101 standard dummy text. Text.  Text! Test: Test; Test?";
            //removing special characters from the text and change all text to lowercase
            String textWithoutSpecialSigns = text.replaceAll("\\.|\\?|\\!|\\:|\\;|\\'", "").toLowerCase();
            String[] arrayOfWords = textWithoutSpecialSigns.split("\\s+");
            int countOfWords = arrayOfWords.length;
            int quantityOfPalindroms = 0;
            int count = countOfWords;

            for (int x = 0; x < count; x++) {
                String test = arrayOfWords[--countOfWords];
                String test2 = new StringBuffer(arrayOfWords[countOfWords]).reverse().toString();

                if (test.equals(test2)) {
                    quantityOfPalindroms++;
                }
                if (x == count - 1) {
                    System.out.println("Количество палиндромов в тексте равно: " + quantityOfPalindroms);
                }
            }

    }
}
