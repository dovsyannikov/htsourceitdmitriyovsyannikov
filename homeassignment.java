package com.company;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import java.util.Scanner;

public class homeassignment {
    public static void main(String[] args) {
        Task1();
        Task2();
        Task3();
        Task4();
        Task4and1();
    }

    public static void Task1() {

        //1. Дано три различных числа, подсчитать сумму квадратов двух наибольших чисел. Результат вывести в консоль.

        //is it ok to solve this problem like that or is there some more efficient way?
        System.out.println("Java. Part B");
        System.out.println();
        System.out.println("Task 1");
        int numberOne = 1;
        int numberTwo = 2;
        int numberThree = 3;

        if ((numberOne <= numberTwo || numberOne <= numberThree) && (numberOne < numberTwo || numberOne < numberThree)) {
            System.out.println("Сумма квадратов двух наибольших чисел равна: " + (numberTwo * numberTwo + numberThree * numberThree));
        } else if ((numberTwo <= numberOne || numberTwo <= numberThree) && (numberTwo < numberOne || numberTwo < numberThree)) {
            System.out.println("Сумма квадратов двух наибольших чисел равна: " + (numberOne * numberOne + numberThree * numberThree));
        } else {
            System.out.println("Сумма квадратов двух наибольших чисел равна: " + (numberOne * numberOne + numberTwo * numberTwo));
        }
        //just for empty line and visibility in console
        System.out.println();
    }

    public static void Task2() {
        //2. Дано три различных числа (a, b, c). Решить квадратное уравнение ax^2 + bx + c = 0. Результат вывести в консоль.

        //ax^2 + bx + c = 0
        System.out.println("Task 2");
        int a = 1;
        int b = 5;
        int c = 3;

        double d = (double) b * b - 4 * a * c;
        if (d > 0) {
            double xOne = (-b + Math.sqrt(d)) / 2 * a;
            double xTwo = (-b - Math.sqrt(d)) / 2 * a;
            System.out.println("Уравнение имеет два корня x1=" + xOne + " , x2=" + xTwo);
        } else if (d == 0) {
            double xOne = -b / 2 * a;
            System.out.println("Уравнение имеет один корень = " + xOne);
        } else {
            System.out.println("Уравнение не имеет корней");
        }
        //just for empty line and visibility in console
        System.out.println();
    }

    public static void Task3() {
        //3. Дан текст. Нужно подсчитать количество слов и предложений в тексте. Результат вывести в консоль.
        System.out.println("Task 3");
        String text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. Text.  Text! Test: Test; Test?";
        String[] words = text.split("(\\w\\s|.\\.|.!|.\\;|.\\:|.\\?)");
        System.out.println("Количество слов в тексте: " + words.length);
        String[] sentences = text.split("(.\\.|.!|.\\;|.\\?)");
        System.out.println("Количество предложений в тексте: " + sentences.length);

        //just for empty line and visibility in console
        System.out.println();
    }

    public static void Task4() {
        //4*. Написать программу, которая будет проверять, является ли строка словом палиндромом.
        // Программа должна определять палиндром, даже если в слове есть буквы разного регистра (например “Шалаш”, “ПоТоП”).
        // Результат проверки (палиндром или нет) выводить в консоль.

        System.out.println("Task 4*");
        String text = "Potop!";
        //removing special characters from the text and change all text to lowercase
        String textWithoutSpecialSigns = text.replaceAll("\\.|\\?|\\!|\\:|\\;|\\'", "").toLowerCase();
        if (textWithoutSpecialSigns.equals(new StringBuffer(textWithoutSpecialSigns).reverse().toString())) {
            System.out.println("Строка является словом палиндромом");
        } else {
            System.out.println("Строка не является словом палиндромом");
        }
        //just for empty line and visibility in console
        System.out.println();
    }

    public static void Task4and1() {
//        4*. Написать программу, которая будет проверять, является ли строка словом палиндромом. Программа должна определять палиндром, даже если в
//        слове есть буквы разного регистра (например “Шалаш”, “ПоТоП”). Результат проверки (палиндром или нет) выводить в консоль.
//        4.1** Определять не только слова, но и предложения. Учесть, что предложения могут содержать
//        знаки пунктуации (например “Аргентина манит негра!”).

        System.out.println("Task 4.1**");
        String text = "Lorol Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's non 101 standard dummy text. Text.  Text! Test: Test; Test?";
        //removing special characters from the text and change all text to lowercase
        String textWithoutSpecialSigns = text.replaceAll("\\.|\\?|\\!|\\:|\\;|\\'", "").toLowerCase();
        String[] arrayOfWords = textWithoutSpecialSigns.split("\\s+");
        int countOfWords = arrayOfWords.length;
        int quantityOfPalindroms = 0;
        int count = countOfWords;

        for (int x = 0; x < count; x++) {
            String test = arrayOfWords[--countOfWords];
            String test2 = new StringBuffer(arrayOfWords[countOfWords]).reverse().toString();

            if (test.equals(test2)) {
                quantityOfPalindroms++;
            }
            if (x == count - 1) {
                System.out.println("Количество палиндромов в тексте равно: " + quantityOfPalindroms);
            }
        }
    }
}
